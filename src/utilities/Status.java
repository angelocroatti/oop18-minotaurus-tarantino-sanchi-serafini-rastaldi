package utilities;

/**
 * Enum representing the type of fixed parts of the maze.
 * Andrea Serafini.
 *
 */
public enum Status {
    /**
     * Type of fixed parts of the maze.
     */
    SIEPE, TEMPIO, ARRIVO, PARTENZA, VUOTO;
}
